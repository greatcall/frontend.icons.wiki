# Icons
Provides configuration and helpers to easily load icon fonts into your web project

### How do I install it?
`install-package GreatCall.FrontEnd.Icons`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.icons.wiki/wiki).
